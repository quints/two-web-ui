class RegisterGrpForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {grpName: '', grpEmail: ''};

        this.onInputChanged = this.onInputChanged.bind(this);

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    onInputChanged(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    handleSubmit(event) {        
        event.preventDefault();

        //this is not good practice, but hey, I dont how to build capability of services in react. 
        //Once I know, I will have this code part of service component in react.
        //e.g. validation service, data fetch service
        if(this.state.grpName && this.state.grpEmail && this.state.grpName.length > 8 && 
            this.state.grpEmail && this.state.grpEmail.length > 4 && this.state.grpEmail.indexOf("@" !== -1) && this.state.grpEmail.indexOf(".") !== -1)
            {
                fetch("http://localhost:3013/grp", {
                    method: "POST",
                    body: JSON.stringify({
                        groupName: this.state.grpName,
                        groupEmail: this.state.grpEmail
                    }),
                    headers: {
                        "Content-Type": "application/json"
                    }
                    //,credentials: "same-origin"
                }).then(
                    (response) => {
                        if(response.status === 201){
                            this.setState({registeredSuccessfully: true});
                        }
                    },
                    (response) => {
                        this.setState({registeredUnsuccessfully: true});
                    }
                );
            }
        else{
            this.setState({registeredSuccessfully: false});
            this.setState({registeredUnsuccessfully: true});
        }
    }


    render() {
        return (
            <div className="box">
                <form onSubmit={this.handleSubmit}>
                    <p className="control">
                        <input name="grpName" className="input" type="text" placeholder="Enter name for community..." 
                            value={this.state.grpName} onChange={this.onInputChanged} />
                    </p>
                    <p className="control">
                        <input name="grpEmail" className="input" type="text" placeholder="Enter community / super-admin email..."
                            value={this.state.grpEmail} onChange={this.onInputChanged} />
                    </p>                    
                    <p className="control">
                        <input type="submit" value="Submit" className="button is-primary" />                        
                    </p>         
                    { this.state.registeredSuccessfully ? <Notification isSuccess message="Community Successfully Registered" /> : null }
                    { this.state.registeredUnsuccessfully ? <Notification isFailure message="Failed to register community"/> : null }           
                </form>
            </div>
        );  
            
    }
}

const registerGrpFormEl = <RegisterGrpForm />;

ReactDOM.render(
    registerGrpFormEl,
    document.getElementById("registerGrpForm")
);