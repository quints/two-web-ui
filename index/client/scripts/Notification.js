class Notification extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return (
            <div className={"notification " + (this.props.isSuccess? "is-success" : "") + " " +  (this.props.isFailure? "is-danger" : "")}>
                <button className="delete"></button>
                {this.props.message}
            </div>
        );
    }
}