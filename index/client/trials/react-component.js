// class Welcome extends React.Component {
//   render() {
//     return <h1>Hello from React Component, {this.props.name}</h1>;
//   }
// }

// #DOM
// script(src="dist/index/trials/dom.js")
// #SimpleReact
// script(src="dist/index/trials/simple-react.js")
// #ReactComponent
// script(src="dist/index/trials/react-component.js")

var Welcome = function(props){
    return <h1>Hello from React Component, {props.name}</h1>;
};


const el = <Welcome name="sandeep" />;

ReactDOM.render(el, document.getElementById("ReactComponent"));