const express = require("express");

const webui = express();

webui.use(express.static("public"));

var viewsDirectories = [
    "index/server/views"
];
webui.set("views", viewsDirectories);
webui.set("view engine", "pug");


var indexRoute = require("./index/server/routes/indexRoute");
webui.use("/", indexRoute);

webui.listen("3026");